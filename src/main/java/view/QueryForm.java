package view;

import entities.Query;
import entities.QueryFieldType;
import sql.SQL;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Vector;

public class QueryForm {
    public JPanel mainPanel;
    public JButton allQueriesButton;
    public JButton requestButton;
    public JTable table;
    public JPanel optionsPane;
    public JLabel statusLabel;
    public JScrollPane scrollPane;

    public QueryForm(JFrame frame, MainMenuForm mainMenuForm, Query currentQuery, SQL sql) {
        mainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), currentQuery.getDisplayName(), TitledBorder.LEFT, TitledBorder.BELOW_TOP, this.$$$getFont$$$(null, Font.BOLD, 16, mainPanel.getFont()), null));
        mainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), currentQuery.getDisplayName(), TitledBorder.LEFT, TitledBorder.BELOW_TOP, this.$$$getFont$$$(null, Font.BOLD, 16, mainPanel.getFont()), null));
        optionsPane.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridy = 0;
        gbc.insets = new Insets(8, 8, 8, 8);

        ArrayList<Component> fields = new ArrayList<>();
        for (var parameterEntry : currentQuery.getParameters().entrySet()) {
            if (parameterEntry.getValue() != QueryFieldType.DUPLICATE) {
                gbc.gridx = 0;
                optionsPane.add(new JLabel(parameterEntry.getKey()[0]), gbc);
                gbc.gridx++;

                Component field = getProperField(parameterEntry.getValue(), parameterEntry.getKey());
                fields.add(field);

                optionsPane.add(field, gbc);
                gbc.gridy++;
            }
        }

        this.requestButton = new JButton("Request");
        optionsPane.add(requestButton, gbc);
        gbc.gridy++;

        allQueriesButton.addActionListener(e -> {
            frame.setContentPane(mainMenuForm.mainPanel);

            frame.setResizable(false);
            frame.setSize(new Dimension(800, 600));
            frame.revalidate();
            frame.getContentPane().update(frame.getContentPane().getGraphics());
        });

        requestButton.addActionListener(e -> {
            ArrayList<Object> params = new ArrayList<>();
            ArrayList<QueryFieldType> paramsTypes = new ArrayList<>();
            String orderOption = null;

            int i = 0;
            for (var parameterEntry : currentQuery.getParameters().entrySet()) {
                if (parameterEntry.getValue() == QueryFieldType.TEXT) {
                    params.add(((JFormattedTextField) fields.get(i)).getText());
                    paramsTypes.add(parameterEntry.getValue());
                } else if (parameterEntry.getValue() == QueryFieldType.ORDER_OPTION) {
                    orderOption = ((JComboBox<String>) fields.get(i)).getSelectedItem().toString();
                } else if (parameterEntry.getValue() == QueryFieldType.NUMBER) {
                    try {
                        params.add(Integer.parseInt(((JFormattedTextField) fields.get(i)).getText()));
                        paramsTypes.add(parameterEntry.getValue());
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(frame, "Enter valid number", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else if (parameterEntry.getValue() == QueryFieldType.DROPDOWN) {
                    String fieldText = ((JComboBox<String>) fields.get(i)).getSelectedItem().toString();
                    params.add(fieldText);
                    paramsTypes.add(QueryFieldType.TEXT);
                } else if (parameterEntry.getValue() == QueryFieldType.DATETIME) {
                    params.add(((JFormattedTextField) fields.get(i)).getText());
                    paramsTypes.add(QueryFieldType.TEXT);
                } else if (parameterEntry.getValue() == QueryFieldType.DUPLICATE) {
                    int idx = Integer.parseInt(parameterEntry.getKey()[0]);
                    params.add(params.get(idx));
                    paramsTypes.add(paramsTypes.get(idx));
                }

                i++;
            }

            try {
                var resultSet = sql.executeQuery(currentQuery.getStatement(), paramsTypes, params, orderOption);
                table = new JTable(buildTableModel(resultSet));

                scrollPane.setViewportView(table);
                mainPanel.update(mainPanel.getGraphics());
                frame.revalidate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
            }
        });
    }

    private Component getProperField(QueryFieldType type, String[] parameters) {
        switch (type) {
            case NUMBER:
                NumberFormat integerFieldFormatter = NumberFormat.getIntegerInstance();
                integerFieldFormatter.setGroupingUsed(false);

                var integerField = new JFormattedTextField(integerFieldFormatter);
                integerField.setColumns(20);
                return integerField;
            case DROPDOWN:
            case ORDER_OPTION:
                var dropdown = new JComboBox<String>();
                for (int i = 1; i < parameters.length; i++) {
                    dropdown.addItem(parameters[i]);
                }
                return dropdown;
            case DATETIME:
                try {
                    MaskFormatter mf = new MaskFormatter("##/##/#### ##:##:##");
                    mf.setPlaceholderCharacter('0');
                    var jFormattedTextField = new JFormattedTextField(mf);
                    jFormattedTextField.setColumns(20);
                    return jFormattedTextField;
                } catch (ParseException ignore) {
                }
            case TEXT:
            default:
                var tf = new JFormattedTextField();
                tf.setColumns(20);
                return tf;
        }
    }

    public static DefaultTableModel buildTableModel(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<String>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<Object>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }

        return new DefaultTableModel(data, columnNames);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 3, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.setBackground(new Color(-1));
        mainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Query Name", TitledBorder.LEFT, TitledBorder.BELOW_TOP, this.$$$getFont$$$(null, Font.BOLD, 16, mainPanel.getFont()), new Color(-16777216)));
        allQueriesButton = new JButton();
        allQueriesButton.setText("All Queries");
        mainPanel.add(allQueriesButton, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        optionsPane = new JPanel();
        optionsPane.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(optionsPane, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        optionsPane.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        scrollPane = new JScrollPane();
        mainPanel.add(scrollPane, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 3, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        table = new JTable();
        scrollPane.setViewportView(table);
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
