package view;

import entities.*;
import sql.SQL;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.text.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class AddRowForm {
    public JPanel mainPanel;
    public JButton saveButton;
    public JButton cancelButton;
    public JScrollPane scrollPane;
    public JPanel editingPanel;
    public JLabel progressLabel;

    public AddRowForm(JFrame frame, TableForm tableForm, Table table, SQL sql) {
        mainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Add Row to " + table.getDisplayName(), TitledBorder.LEFT, TitledBorder.BELOW_TOP, this.$$$getFont$$$(null, Font.BOLD, 16, mainPanel.getFont()), null));
        editingPanel.setLayout(new GridBagLayout());
        progressLabel.setVisible(false);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridy = 0;
        gbc.insets = new Insets(8, 8, 8, 8);

        ArrayList<JFormattedTextField> textFields = new ArrayList<>();
        for (Field field : table.getFields()) {
            if (field.getFieldType() != FieldType.AUTOINCREMENT) {
                gbc.gridx = 0;
                JLabel label = new JLabel(field.getDisplayName() + (field.getFieldType() == FieldType.GENDER ? " (M/F)" : "") +
                        (field.getFieldType() == FieldType.BOOL ? " (T/F)" : "")
                                + ((field.isNotNull()) ? " *" : ""));
                label.setFont(this.$$$getFont$$$(null, Font.PLAIN, 14, mainPanel.getFont()));

                editingPanel.add(label, gbc);

                gbc.gridx++;
                var textField = pickProperElement(field);
                textFields.add(textField);
                editingPanel.add(textField, gbc);
                gbc.gridy++;
            }
        }

        frame.pack();

        cancelButton.addActionListener(e -> {
            frame.setContentPane(tableForm.mainPanel);
            frame.setResizable(true);
            frame.revalidate();
            frame.getContentPane().update(frame.getContentPane().getGraphics());
        });

        saveButton.addActionListener(e -> {
            progressLabel.setVisible(true);

            frame.revalidate();
            mainPanel.update(mainPanel.getGraphics());

            Row row = new Row();
            int i = 0;
            for (Field field : table.getFields()) {
                if (field.getFieldType() == FieldType.AUTOINCREMENT) {
                    continue;
                }

                String fieldText = textFields.get(i).getText();

                if (field.getFieldType() == FieldType.GENDER && !fieldText.equals("M") && !fieldText.equals("F")) {
                    JOptionPane.showMessageDialog(frame, "Field " + field.getDisplayName() + ": value must be either 'M' or 'F'", "Error", JOptionPane.ERROR_MESSAGE);
                    progressLabel.setVisible(false);
                    return;
                }

                if (field.getFieldType() == FieldType.BOOL && !fieldText.equals("T") && !fieldText.equals("F")) {
                    JOptionPane.showMessageDialog(frame, "Field " + field.getDisplayName() + ": value must be either 'T' or 'F'", "Error", JOptionPane.ERROR_MESSAGE);
                    progressLabel.setVisible(false);
                    return;
                }

                if (fieldText.isEmpty() || fieldText.equals("00/00/0000 00:00:00")) {
                    i++;
                    continue;
                }

                row.add(new Value(field, fieldText));

                i++;
            }

            String errorMessage = sql.addRow(table, row);

            if (errorMessage.equals("")) {
                tableForm.update();
                frame.setContentPane(tableForm.mainPanel);
                frame.setResizable(true);
                frame.revalidate();
                frame.getContentPane().update(frame.getContentPane().getGraphics());
            }
            else {
                JOptionPane.showMessageDialog(frame, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
                progressLabel.setVisible(false);
            }
        });
    }

    private JFormattedTextField pickProperElement(Field field) {
        switch (field.getFieldType()) {
            case NUM:
                NumberFormat integerFieldFormatter = NumberFormat.getIntegerInstance();
                integerFieldFormatter.setGroupingUsed(false);

                var integerField = new JFormattedTextField(integerFieldFormatter);
                integerField.setColumns(20);
                return integerField;
            case STRING:
                var tf = new JFormattedTextField();
                tf.setColumns(20);
                return tf;
            case BOOL:
                var boolField = new JFormattedTextField();
                boolField.setColumns(20);

                return boolField;
            case GENDER:
                var genderField = new JFormattedTextField();
                genderField.setColumns(20);

                return genderField;
            case DATETIME:
                try {
                    MaskFormatter mf = new MaskFormatter("##/##/#### ##:##:##");
                    mf.setPlaceholderCharacter('0');
                    var jFormattedTextField = new JFormattedTextField(mf);
                    jFormattedTextField.setColumns(20);
                    return jFormattedTextField;
                } catch (ParseException ignore) {
                }
            default:
                return new JFormattedTextField();
        }
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Add Row to", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.BELOW_TOP, this.$$$getFont$$$(null, Font.BOLD, 16, mainPanel.getFont()), null));
        cancelButton = new JButton();
        cancelButton.setText("Cancel");
        mainPanel.add(cancelButton, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        saveButton = new JButton();
        saveButton.setText("Save");
        mainPanel.add(saveButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        scrollPane = new JScrollPane();
        mainPanel.add(scrollPane, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        editingPanel = new JPanel();
        editingPanel.setLayout(new GridBagLayout());
        scrollPane.setViewportView(editingPanel);
        progressLabel = new JLabel();
        progressLabel.setText("Adding new row...");
        mainPanel.add(progressLabel, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
