package view;

import sql.Model;
import sql.SQL;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.ArrayList;

public class MainMenuForm {
    public JPanel mainPanel;
    public JPanel flightsPanel;
    public JPanel staffPanel;
    public JPanel salesPanel;
    public JButton allStaffButton;
    public JButton administratorsButton;
    public JButton pilotsButton;
    public JButton techniciansButton;
    public JButton serviceStaffButton;
    public JButton departmentsButton;
    public JButton brigadesButton;
    public JButton medicalCheckupsButton;
    public JButton ticketSalesButton;
    public JButton ticketReturnsButton;
    public JButton passengersButton;
    public JButton planesButton;
    public JButton planeTypesButton;
    public JButton flightsButton;
    public JButton timetableButton;
    public JButton timetableHistoryButton;
    public JButton inspectionHistoryButton;
    public JButton repairHistoryButton;
    public JButton flightStatusButton;
    public JButton flightTypeButton;
    public JButton airportButton;
    public JButton routeButton;
    public JTabbedPane tabbedPane;
    public JPanel tablesPanel;
    public JPanel queriesPane;

    private final JFrame frame;
    private final SQL sql;

    public MainMenuForm(JFrame frame, SQL sql) {
        this.frame = frame;
        this.sql = sql;

        initTablesPane();
        initQueriesPane();
    }

    private void initQueriesPane() {
        queriesPane.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridy = 0;
        gbc.insets = new Insets(2, 2, 2, 2);

        for (var entry : Model.queries.entrySet()) {
            JButton button = new JButton(entry.getKey());
            button.addActionListener(e -> {
                frame.setContentPane(new QueryForm(frame, this, entry.getValue(), sql).mainPanel);
                setSizingOptions();
            });

            queriesPane.add(button, gbc);
            gbc.gridy++;
        }
    }

    private void initTablesPane() {
        allStaffButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("WORKER_INFO"), sql).mainPanel);
            setSizingOptions();
        });
        administratorsButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("ADMINISTRATOR"), sql).mainPanel);
            setSizingOptions();
        });
        pilotsButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("PILOT"), sql).mainPanel);
            setSizingOptions();
        });
        techniciansButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("TECHNICIAN"), sql).mainPanel);
            setSizingOptions();
        });
        serviceStaffButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("SERVICE_STAFFER"), sql).mainPanel);
            setSizingOptions();
        });
        departmentsButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("DEPARTMENT"), sql).mainPanel);
            setSizingOptions();
        });
        brigadesButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("BRIGADE"), sql).mainPanel);
            setSizingOptions();
        });
        medicalCheckupsButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("MEDICAL_CHECKUP_HISTORY"), sql).mainPanel);
            setSizingOptions();
        });
        ticketSalesButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("TICKET"), sql).mainPanel);
            setSizingOptions();
        });
        ticketReturnsButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("TICKET_RETURNS"), sql).mainPanel);
            setSizingOptions();
        });
        passengersButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("PASSENGER"), sql).mainPanel);
            setSizingOptions();
        });
        planesButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("PLANE"), sql).mainPanel);
            setSizingOptions();
        });
        planeTypesButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("PLANE_TYPE"), sql).mainPanel);
            setSizingOptions();
        });
        flightsButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("FLIGHT"), sql).mainPanel);
            setSizingOptions();
        });
        timetableButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("TIMETABLE"), sql).mainPanel);
            setSizingOptions();
        });
        timetableHistoryButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("TIMETABLE_HISTORY"), sql).mainPanel);
            setSizingOptions();
        });
        inspectionHistoryButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("INSPECTION_HISTORY"), sql).mainPanel);
            setSizingOptions();
        });
        repairHistoryButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("REPAIR_HISTORY"), sql).mainPanel);
            setSizingOptions();
        });
        flightStatusButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("FLIGHT_STATUS"), sql).mainPanel);
            setSizingOptions();
        });
        flightTypeButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("FLIGHT_TYPE"), sql).mainPanel);
            setSizingOptions();
        });
        airportButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("AIRPORT"), sql).mainPanel);
            setSizingOptions();
        });
        routeButton.addActionListener(e -> {
            frame.setContentPane(new TableForm(frame, this, Model.tables.get("ROUTE"), sql).mainPanel);
            setSizingOptions();
        });
    }

    private void setSizingOptions() {
        frame.setResizable(true);
        frame.setMinimumSize(frame.getSize());
        frame.revalidate();
        frame.getContentPane().update(frame.getContentPane().getGraphics());
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 2, new Insets(0, 10, 10, 10), -1, -1));
        mainPanel.setBackground(new Color(-1));
        tabbedPane = new JTabbedPane();
        tabbedPane.setBackground(new Color(-1));
        mainPanel.add(tabbedPane, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(200, 200), null, 0, false));
        tablesPanel = new JPanel();
        tablesPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        tabbedPane.addTab("Tables", tablesPanel);
        flightsPanel = new JPanel();
        flightsPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 4, new Insets(0, 0, 0, 0), -1, -1));
        flightsPanel.setBackground(new Color(-1973791));
        tablesPanel.add(flightsPanel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        flightsPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Flight Management", TitledBorder.LEFT, TitledBorder.BELOW_TOP, this.$$$getFont$$$(null, Font.BOLD, 16, flightsPanel.getFont()), null));
        planesButton = new JButton();
        Font planesButtonFont = this.$$$getFont$$$(null, -1, 14, planesButton.getFont());
        if (planesButtonFont != null) planesButton.setFont(planesButtonFont);
        planesButton.setText("Planes");
        flightsPanel.add(planesButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        planeTypesButton = new JButton();
        Font planeTypesButtonFont = this.$$$getFont$$$(null, -1, 14, planeTypesButton.getFont());
        if (planeTypesButtonFont != null) planeTypesButton.setFont(planeTypesButtonFont);
        planeTypesButton.setText("Plane Types");
        flightsPanel.add(planeTypesButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        flightsButton = new JButton();
        Font flightsButtonFont = this.$$$getFont$$$(null, -1, 14, flightsButton.getFont());
        if (flightsButtonFont != null) flightsButton.setFont(flightsButtonFont);
        flightsButton.setText("Flights");
        flightsPanel.add(flightsButton, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        timetableButton = new JButton();
        Font timetableButtonFont = this.$$$getFont$$$(null, -1, 14, timetableButton.getFont());
        if (timetableButtonFont != null) timetableButton.setFont(timetableButtonFont);
        timetableButton.setText("Timetable");
        flightsPanel.add(timetableButton, new com.intellij.uiDesigner.core.GridConstraints(0, 3, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        inspectionHistoryButton = new JButton();
        Font inspectionHistoryButtonFont = this.$$$getFont$$$(null, -1, 14, inspectionHistoryButton.getFont());
        if (inspectionHistoryButtonFont != null) inspectionHistoryButton.setFont(inspectionHistoryButtonFont);
        inspectionHistoryButton.setText("Inspection History");
        flightsPanel.add(inspectionHistoryButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        repairHistoryButton = new JButton();
        Font repairHistoryButtonFont = this.$$$getFont$$$(null, -1, 14, repairHistoryButton.getFont());
        if (repairHistoryButtonFont != null) repairHistoryButton.setFont(repairHistoryButtonFont);
        repairHistoryButton.setText("Repair History");
        flightsPanel.add(repairHistoryButton, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        timetableHistoryButton = new JButton();
        Font timetableHistoryButtonFont = this.$$$getFont$$$(null, -1, 14, timetableHistoryButton.getFont());
        if (timetableHistoryButtonFont != null) timetableHistoryButton.setFont(timetableHistoryButtonFont);
        timetableHistoryButton.setText("Timetable History");
        flightsPanel.add(timetableHistoryButton, new com.intellij.uiDesigner.core.GridConstraints(2, 3, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        airportButton = new JButton();
        Font airportButtonFont = this.$$$getFont$$$(null, -1, 14, airportButton.getFont());
        if (airportButtonFont != null) airportButton.setFont(airportButtonFont);
        airportButton.setText("Airports");
        flightsPanel.add(airportButton, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        routeButton = new JButton();
        Font routeButtonFont = this.$$$getFont$$$(null, -1, 14, routeButton.getFont());
        if (routeButtonFont != null) routeButton.setFont(routeButtonFont);
        routeButton.setText("Routes");
        flightsPanel.add(routeButton, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        flightTypeButton = new JButton();
        Font flightTypeButtonFont = this.$$$getFont$$$(null, -1, 14, flightTypeButton.getFont());
        if (flightTypeButtonFont != null) flightTypeButton.setFont(flightTypeButtonFont);
        flightTypeButton.setText("Flight Types");
        flightsPanel.add(flightTypeButton, new com.intellij.uiDesigner.core.GridConstraints(3, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        flightStatusButton = new JButton();
        Font flightStatusButtonFont = this.$$$getFont$$$(null, -1, 14, flightStatusButton.getFont());
        if (flightStatusButtonFont != null) flightStatusButton.setFont(flightStatusButtonFont);
        flightStatusButton.setText("Flight Status Types");
        flightsPanel.add(flightStatusButton, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        staffPanel = new JPanel();
        staffPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(5, 3, new Insets(0, 0, 0, 0), -1, -1));
        staffPanel.setBackground(new Color(-1973791));
        tablesPanel.add(staffPanel, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        staffPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Staff", TitledBorder.LEFT, TitledBorder.BELOW_TOP, this.$$$getFont$$$(null, Font.BOLD, 16, staffPanel.getFont()), null));
        allStaffButton = new JButton();
        Font allStaffButtonFont = this.$$$getFont$$$(null, -1, 14, allStaffButton.getFont());
        if (allStaffButtonFont != null) allStaffButton.setFont(allStaffButtonFont);
        allStaffButton.setText("All Staff");
        staffPanel.add(allStaffButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        administratorsButton = new JButton();
        Font administratorsButtonFont = this.$$$getFont$$$(null, -1, 14, administratorsButton.getFont());
        if (administratorsButtonFont != null) administratorsButton.setFont(administratorsButtonFont);
        administratorsButton.setText("Administrators");
        staffPanel.add(administratorsButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        pilotsButton = new JButton();
        Font pilotsButtonFont = this.$$$getFont$$$(null, -1, 14, pilotsButton.getFont());
        if (pilotsButtonFont != null) pilotsButton.setFont(pilotsButtonFont);
        pilotsButton.setText("Pilots");
        staffPanel.add(pilotsButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        techniciansButton = new JButton();
        Font techniciansButtonFont = this.$$$getFont$$$(null, -1, 14, techniciansButton.getFont());
        if (techniciansButtonFont != null) techniciansButton.setFont(techniciansButtonFont);
        techniciansButton.setText("Technicians");
        staffPanel.add(techniciansButton, new com.intellij.uiDesigner.core.GridConstraints(4, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        serviceStaffButton = new JButton();
        Font serviceStaffButtonFont = this.$$$getFont$$$(null, -1, 14, serviceStaffButton.getFont());
        if (serviceStaffButtonFont != null) serviceStaffButton.setFont(serviceStaffButtonFont);
        serviceStaffButton.setText("Service Staff");
        staffPanel.add(serviceStaffButton, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        departmentsButton = new JButton();
        Font departmentsButtonFont = this.$$$getFont$$$(null, -1, 14, departmentsButton.getFont());
        if (departmentsButtonFont != null) departmentsButton.setFont(departmentsButtonFont);
        departmentsButton.setText("Departments");
        staffPanel.add(departmentsButton, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        brigadesButton = new JButton();
        Font brigadesButtonFont = this.$$$getFont$$$(null, -1, 14, brigadesButton.getFont());
        if (brigadesButtonFont != null) brigadesButton.setFont(brigadesButtonFont);
        brigadesButton.setText("Brigades");
        staffPanel.add(brigadesButton, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        medicalCheckupsButton = new JButton();
        Font medicalCheckupsButtonFont = this.$$$getFont$$$(null, -1, 14, medicalCheckupsButton.getFont());
        if (medicalCheckupsButtonFont != null) medicalCheckupsButton.setFont(medicalCheckupsButtonFont);
        medicalCheckupsButton.setText("Medical Checkups");
        staffPanel.add(medicalCheckupsButton, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        salesPanel = new JPanel();
        salesPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        salesPanel.setBackground(new Color(-1973791));
        tablesPanel.add(salesPanel, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        salesPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Sales", TitledBorder.LEFT, TitledBorder.BELOW_TOP, this.$$$getFont$$$(null, Font.BOLD, 16, salesPanel.getFont()), new Color(-16777216)));
        ticketSalesButton = new JButton();
        Font ticketSalesButtonFont = this.$$$getFont$$$(null, -1, 14, ticketSalesButton.getFont());
        if (ticketSalesButtonFont != null) ticketSalesButton.setFont(ticketSalesButtonFont);
        ticketSalesButton.setText("Ticket Sales");
        salesPanel.add(ticketSalesButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        passengersButton = new JButton();
        Font passengersButtonFont = this.$$$getFont$$$(null, -1, 14, passengersButton.getFont());
        if (passengersButtonFont != null) passengersButton.setFont(passengersButtonFont);
        passengersButton.setText("Passengers");
        salesPanel.add(passengersButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        ticketReturnsButton = new JButton();
        Font ticketReturnsButtonFont = this.$$$getFont$$$(null, -1, 14, ticketReturnsButton.getFont());
        if (ticketReturnsButtonFont != null) ticketReturnsButton.setFont(ticketReturnsButtonFont);
        ticketReturnsButton.setText("Ticket Returns");
        salesPanel.add(ticketReturnsButton, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        queriesPane = new JPanel();
        queriesPane.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1, true, false));
        queriesPane.setBackground(new Color(-1));
        tabbedPane.addTab("Queries", queriesPane);
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
