package view;

import entities.Row;
import entities.Table;
import entities.Value;
import sql.SQL;

import javax.swing.table.AbstractTableModel;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class CustomTableModel extends AbstractTableModel {

    private final Table table;
    private final Vector<String> columnNames = new Vector<>();
    private final Vector<Vector<Object>> rows = new Vector<>();
    private final ResultSetMetaData metadata;
    private final SQL sql;
    private final TableForm tableForm;

    public CustomTableModel(ResultSet resultSet, Table table, SQL sql, TableForm tableForm) throws SQLException {
        this.table = table;
        this.sql = sql;
        this.tableForm = tableForm;
        metadata = resultSet.getMetaData();

        for (int i = 0; i < metadata.getColumnCount(); ++i) {
            columnNames.add(table.getFields().get(i).getDisplayName());
        }

        while (resultSet.next()) {
            Vector<Object> row = new Vector<>();
            for (int i = 1; i <= metadata.getColumnCount(); ++i) {
                row.add(resultSet.getObject(i));
            }
            rows.add(row);
        }
    }

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public String getColumnName(int index) {
        return columnNames.get(index);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rows.get(rowIndex).get(columnIndex);
    }

    @Override
    public void setValueAt(Object val, int rowIndex, int columnIndex) {
        tableForm.updateStatusLabel("Updating value...");

        Row row = new Row();
        for (int i = 0; i < columnNames.size(); ++i) {
            row.add(new Value(table.getField(columnNames.get(i)), rows.get(rowIndex).get(i)));
        }

        try {
            sql.updateValue(table, new Value(table.getField(columnNames.get(columnIndex)), val), row);
            rows.get(rowIndex).set(columnIndex, val);
            super.setValueAt(val, rowIndex, columnIndex);
            tableForm.updateStatusLabel(null);
        }
        catch (SQLException ex) {
            tableForm.updateStatusLabel("Could not update chosen row");
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        try {
            return metadata.isWritable(columnIndex + 1);
        }
        catch (SQLException ex) {
            return false;
        }
    }

    public void removeRow(int rowIndex) {
        rows.remove(rowIndex);
        super.fireTableRowsDeleted(rowIndex, rowIndex);
        super.fireTableDataChanged();
    }
}
