package entities;

public class Field {
    private final String name;
    private final String displayName;
    private final boolean notNull;
    private final FieldType fieldType;

    public Field(String name, String displayName, boolean notNull, FieldType type) {
        this.name = name;
        this.displayName = displayName;
        this.notNull = notNull;
        this.fieldType = type;
    }

    public String getName() {
        return name;
    }

    public boolean isNotNull() {
        return notNull;
    }

    public String getDisplayName() {
        return displayName;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public boolean doesNeedQuotes() {
        return !fieldType.equals(FieldType.NUM) && !fieldType.equals(FieldType.AUTOINCREMENT);
    }
}
