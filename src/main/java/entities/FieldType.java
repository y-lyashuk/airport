package entities;

public enum FieldType {
    DATETIME, GENDER, BOOL, STRING, NUM, AUTOINCREMENT
}
