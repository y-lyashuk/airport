package entities;

import java.util.List;

public class Table {
    private final String name;
    private final String displayName;
    private final List<Field> fields;

    public Table(String name, String displayName, List<Field> fields) {
        this.name = name;
        this.displayName = displayName;
        this.fields = fields;
    }

    public String getName() {
        return name;
    }

    public List<Field> getFields() {
        return fields;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Field getField(String name) {
        for (Field field : fields) {
            if (field.getDisplayName().equals(name)) {
                return field;
            }
        }
        return null;
    }
}
