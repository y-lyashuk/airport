package entities;

public enum QueryFieldType {
    DROPDOWN, TEXT, NUMBER, DATETIME, ORDER_OPTION, DUPLICATE
}
