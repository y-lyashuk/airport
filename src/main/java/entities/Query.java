package entities;

import sql.SQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class Query {
    private final String displayName;
    private final String statement;
    private final LinkedHashMap<String[], QueryFieldType> parameters;

    public Query(String displayName, String statement, LinkedHashMap<String[], QueryFieldType> parameters) {
        this.displayName = displayName;
        this.statement = statement;
        this.parameters = parameters;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getStatement() {
        return statement;
    }

    public LinkedHashMap<String[], QueryFieldType> getParameters() {
        return parameters;
    }
}
