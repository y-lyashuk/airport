import view.LoginForm;
import view.MainMenuForm;

import javax.swing.*;

public class AirportDB {
    private static final JFrame frame = new JFrame("AirportDB");
    private static final LoginForm loginForm = new LoginForm(frame);

    public AirportDB() {
        frame.setSize(480, 320);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public void showLoginForm() {
        frame.setContentPane(loginForm.loginPanel);
    }
}
