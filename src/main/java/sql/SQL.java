package sql;

import entities.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

public class SQL implements AutoCloseable {
    private Connection connection = null;

    public SQL(String url, String username, String password) throws SQLException, ClassNotFoundException {
        Class.forName("oracle.jdbc.driver.OracleDriver");

        TimeZone timeZone = TimeZone.getTimeZone("GMT+7");
        TimeZone.setDefault(timeZone);
        Locale.setDefault(Locale.ENGLISH);

        connection = DriverManager.getConnection(url, username, password);

        if (connection == null) {
            throw new SQLException("Could not establish connection with the database");
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public ResultSet executeQuery(String statementStr, ArrayList<QueryFieldType> parameterTypes, ArrayList<Object> preparedParameters, String orderOption) throws SQLException {
        if (orderOption != null) {
            statementStr += "\nORDER BY " + orderOption;
        }

        PreparedStatement statement = connection.prepareStatement(statementStr);
        for (int i = 0; i < preparedParameters.size(); i++) {
            switch (parameterTypes.get(i)) {
                case NUMBER:
                    statement.setInt(i + 1, (int) preparedParameters.get(i));
                    break;
                case TEXT:
                default:
                    statement.setString(i + 1, (String) preparedParameters.get(i));
            }
        }

        return statement.executeQuery();
    }

    public ResultSet getTable(Table table) throws SQLException {
        return runSQLRequest("SELECT * FROM " + table.getName());
    }

    public String addRow(Table table, Row row) {
        StringBuilder request = new StringBuilder("insert into " + table.getName() + "\n");
        request.append("(");

        System.err.println(request.toString());

        boolean isFirst = true;
        for (Value value : row.getValues()) {
            if (!isFirst) {
                request.append(", ");
            }
            else {
                isFirst = false;
            }
            request.append("\"").append(value.getField().getName()).append("\"");
        }
        System.err.println(request.toString());

        request.append(")\nvalues\n(");

        isFirst = true;
        for (Value value : row.getValues()) {
            if (!isFirst) {
                request.append(", ");
            }
            else {
                isFirst = false;
            }

            if (value.getField().getFieldType() == FieldType.DATETIME) {
                request.append("TO_DATE(");
            }

            if (value.getField().getFieldType() != FieldType.NUM) {
                request.append("'");
            }

            request.append(value.getValue());

            if (value.getField().getFieldType() != FieldType.NUM) {
                request.append("'");
            }

            if (value.getField().getFieldType() == FieldType.DATETIME) {
                request.append(", 'DD/MM/YYYY HH24:MI:SS')");
            }
        }
        request.append(")");

        try {
            System.out.println(request.toString());
            runSQLRequest(request.toString());
        }
        catch (SQLException ex) {
            return ex.getMessage();
        }

        return "";
    }

    public String deleteRow(Table table, Row row) {
        StringBuilder request = new StringBuilder("delete from " + table.getName() + " where\n");
        request.append(getWhereCondition(row));

        System.out.println(request);
        try {
            runSQLRequest(request.toString());
        }
        catch (SQLException ex) {
            return ex.getMessage();
        }
        return "";
    }

    public void updateValue(Table table, Value value, Row rowToUpdate) throws SQLException { // todo check
        StringBuilder request = new StringBuilder("update " + table.getName() + "\nset ");
        request.append(value.getField().getName());

        request.append(" = ");

        if (value.getField().getFieldType() == FieldType.DATETIME) {
            request.append("TO_TIMESTAMP(");
        }

        if (value.getField().getFieldType() != FieldType.NUM && value.getField().getFieldType() != FieldType.AUTOINCREMENT) {
            request.append("'");
        }

        request.append(value.getValue());

        if (value.getField().getFieldType() != FieldType.NUM && value.getField().getFieldType() != FieldType.AUTOINCREMENT) {
            request.append("'");
        }

        if (value.getField().getFieldType() == FieldType.DATETIME) {
            request.append(", 'YYYY-MM-DD HH24:MI:SS.FF')");
        }

        request.append("\n where \n").append(getWhereCondition(rowToUpdate));

        System.out.println(request);

        runSQLRequest(request.toString());
    }

    public ResultSet runSQLRequest(String request) throws SQLException {
        PreparedStatement preStatement = connection.prepareStatement(request);
        return preStatement.executeQuery();
    }

    private String getWhereCondition(Row row) {
        StringBuilder request = new StringBuilder();
        boolean isFirst = true;
        for (Value value : row.getValues()) {
            if (value.getValue() == null) {
                continue;
            }
            if (!isFirst) {
                request.append(" and\n");
            }
            else {
                isFirst = false;
            }
            request.append("\"").append(value.getField().getName()).append("\"")
                    .append(" = ");

            if (value.getField().getFieldType() == FieldType.DATETIME) {
                request.append("TO_TIMESTAMP(");
            }

            if (value.getField().getFieldType() != FieldType.NUM && value.getField().getFieldType() != FieldType.AUTOINCREMENT) {
                request.append("'");
            }

            request.append(value.getValue());

            if (value.getField().getFieldType() != FieldType.NUM && value.getField().getFieldType() != FieldType.AUTOINCREMENT) {
                request.append("'");
            }

            if (value.getField().getFieldType() == FieldType.DATETIME) {
                request.append(", 'YYYY-MM-DD HH24:MI:SS.FF')");
            }
        }
        return request.toString();
    }

    @Override
    public void close() throws SQLException {
        if (connection != null){
            connection.close();
        }
    }
}
