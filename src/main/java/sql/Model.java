package sql;

import entities.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class Model {
    public final static HashMap<String, Table> tables = new HashMap<>()
    {{
            put("ADMINISTRATOR",
                    new Table(
                    "ADMINISTRATOR",
                    "Administrators",
                    Arrays.asList(
                            new Field("WORKER_ID", "Worker ID", true, FieldType.NUM), // ref,
                            new Field("HAS_ADMINISTRATIVE_EXPERIENCE", "Has Administrative Experience", true, FieldType.BOOL)
                    )
            ));
            put("AIRPORT",
                    new Table(
                    "AIRPORT",
                    "Airports",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("AIRPORT_NAME", "Airport Name", true, FieldType.STRING),
                            new Field("COUNTRY", "Country", true,FieldType.STRING),
                            new Field("CITY", "City", true, FieldType.STRING)
                    )
            ));
            put("BRIGADE",
                    new Table(
                    "BRIGADE",
                    "Brigades",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("DEPARTMENT_ID", "Department ID", true, FieldType.NUM) // ref
                    )
            ));
            put("DAYS_OF_DEPARTURE",
                    new Table(
                    "DAYS_OF_DEPARTURE",
                    "Days of Departure",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("SUNDAY", "Sunday", true, FieldType.BOOL),
                            new Field("MONDAY", "Monday", true,FieldType.BOOL),
                            new Field("TUESDAY", "Tuesday", true,FieldType.BOOL),
                            new Field("WEDNESDAY", "Wednesday", true, FieldType.BOOL),
                            new Field("THURSDAY", "Thursday", true,FieldType.BOOL),
                            new Field("FRIDAY", "Friday", true, FieldType.BOOL),
                            new Field("SATURDAY", "Saturday", true, FieldType.BOOL)
                    )
            ));
            put("DEPARTMENT",
                    new Table(
                    "DEPARTMENT",
                    "Departments",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("DEPARTMENT_NAME", "Department Name", true, FieldType.STRING),
                            new Field("MANAGER_ID", "Manager ID", true, FieldType.NUM) // ref
                    )
            ));
            put("FLIGHT",
                    new Table(
                    "FLIGHT",
                    "Flights",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("FLIGHT_TYPE_ID", "Flight Type ID", true, FieldType.NUM), //ref
                            new Field("FLIGHT_NUMBER", "Flight Number", true, FieldType.STRING),
                            new Field("PLANE_TYPE_ID", "Plane Type ID", true, FieldType.NUM), // ref
                            new Field("ROUTE_ID", "Route ID", true, FieldType.NUM), // ref
                            new Field("TIME_OF_DEPARTURE", "Time of Departure", false, FieldType.DATETIME),
                            new Field("COST", "Cost", true, FieldType.NUM),
                            new Field("DURATION", "Duration", true, FieldType.NUM)
                    )
            ));
            put("FLIGHT_STATUS",
                    new Table(
                    "FLIGHT_STATUS",
                    "Flight Statuses",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("NAME", "Name", true, FieldType.STRING)
                    )
            ));
            put("FLIGHT_TYPE",
                    new Table(
                    "FLIGHT_TYPE",
                    "Flight Types",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("NAME", "Name", true, FieldType.STRING)
                    )
            ));
            put("INSPECTION_HISTORY",
                    new Table(
                    "INSPECTION_HISTORY",
                    "Inspection History",
                    Arrays.asList(
                            new Field("PLANE_ID", "Plane ID", true, FieldType.NUM), // ref
                            new Field("INSPECTION_DATE", "Inspection Date", true, FieldType.DATETIME),
                            new Field("IS_SUCCESSFUL", "Is Successful", true, FieldType.BOOL)
                    )
            ));
            put("MEDICAL_CHECKUP_HISTORY",
                    new Table(
                    "MEDICAL_CHECKUP_HISTORY",
                    "Medical Checkup History",
                    Arrays.asList(
                            new Field("PILOT_ID", "Pilot ID", true, FieldType.NUM), //ref
                            new Field("CHECKUP_DATE", "Checkup Date", true, FieldType.DATETIME),
                            new Field("IS_SUCCESSFUL", "Is Successful", true, FieldType.BOOL)
                    )
            ));
            put("PASSENGER",
                    new Table(
                    "PASSENGER",
                    "Passengers",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("FIRST_NAME", "First Name", true, FieldType.STRING),
                            new Field("SECOND_NAME", "Second Name", false,FieldType.STRING),
                            new Field("LAST_NAME", "Last Name", true, FieldType.STRING),
                            new Field("BIRTH_DATE", "Birth Date", true, FieldType.DATETIME),
                            new Field("GENDER", "Gender", true, FieldType.GENDER)
                    )
            ));
            put("PILOT",
                    new Table(
                    "PILOT",
                    "Pilots",
                    Arrays.asList(
                            new Field("WORKER_ID", "Worker ID", true, FieldType.NUM), // ref
                            new Field("LICENSE_NUMBER", "License Number", true, FieldType.STRING) // todo check if string
                    )
            ));
            put("PLANE",
                    new Table(
                    "PLANE",
                    "Planes",
                    Arrays.asList(
                            new Field("PLANE_ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("PLANE_TYPE_ID", "Plane Type ID", true, FieldType.NUM), // ref
                            new Field("BRIGADE_ID", "Brigade ID", true, FieldType.NUM), // ref
                            new Field("START_OF_USE_DATE", "Start of Use Date", true, FieldType.DATETIME),
                            new Field("BASE_AIRPORT_ID", "Base Airport ID", true, FieldType.NUM) // ref
                    )
            ));
            put("PLANE_TYPE",
                    new Table(
                    "PLANE_TYPE",
                    "Plane Types",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("MANUFACTURER", "Manufacturer", true, FieldType.STRING),
                            new Field("MODEL", "Model", true, FieldType.STRING),
                            new Field("SEATS_COUNT", "Seats Count", true, FieldType.NUM)
                    )
            ));
            put("REPAIR_HISTORY",
                    new Table(
                    "REPAIR_HISTORY",
                    "Repair History",
                    Arrays.asList(
                            new Field("PLANE_ID", "Plane ID", true, FieldType.NUM), // ref
                            new Field("REPAIR_DATE", "Repair Date", true,FieldType.DATETIME),
                            new Field("Description", "Description", false, FieldType.STRING)
                    )
            ));
            put("ROUTE",
                    new Table(
                    "ROUTE",
                    "Routes",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("DEPARTURE_ID", "Departure ID", true, FieldType.NUM), // ref
                            new Field("TRANSFER ID", "Transfer ID", false, FieldType.NUM), // ref
                            new Field("ARRIVAL_ID", "Arrival ID", true, FieldType.NUM) // ref
                    )
            ));
            put("SERVICE_STAFFER",
                    new Table(
                    "SERVICE_STAFFER",
                    "Service Staff",
                    Arrays.asList(
                            new Field("WORKER_ID", "Worker ID", true, FieldType.NUM), // ref
                            new Field("TOOK_ADDITIONAL_TRAINING", "Took Additional Training", true, FieldType.BOOL)
                    )
            ));
            put("TECHNICIAN",
                    new Table(
                    "TECHNICIAN",
                    "Technicians",
                    Arrays.asList(
                            new Field("WORKER_ID", "Worker ID", true, FieldType.NUM), // ref
                            new Field("HAS_UNIVERSITY_DEGREE", "Has University Degree", true, FieldType.BOOL)
                    )
            ));
            put("TICKET",
                    new Table(
                    "TICKET",
                    "Ticket Sales",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("PASSENGER_ID", "Passenger ID", true, FieldType.NUM), // ref
                            new Field("TIMETABLE_ID", "Timetable ID", true, FieldType.NUM), // ref
                            new Field("AMOUNT_OF_LUGGAGE", "Amount of Luggage", true, FieldType.NUM),
                            new Field("SEAT", "Seat", true, FieldType.NUM),
                            new Field("TIME_OF_PURCHASE", "Time of Purchase", true, FieldType.DATETIME)
                    )
            ));
            put("TICKET_RETURNS",
                    new Table(
                    "TICKET_RETURNS",
                    "Ticket Returns",
                    Arrays.asList(
                            new Field("TICKET_ID", "Ticket ID", true, FieldType.NUM), // ref
                            new Field("TIME_OF_RETURN", "Time of Return", true, FieldType.DATETIME)
                    )
            ));
            put("TIMETABLE",
                    new Table(
                    "TIMETABLE",
                    "Timetable",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("FLIGHT_ID", "Flight ID", true, FieldType.NUM), //ref
                            new Field("Plane_ID", "Plane ID", true, FieldType.NUM), //ref
                            new Field("INITIAL_DEPARTURE", "Initial Departure", true, FieldType.DATETIME),
                            new Field("INITIAL_ARRIVAL", "Initial Arrival", true, FieldType.DATETIME)
                    )
            ));
            put("TIMETABLE_HISTORY",
                    new Table(
                    "TIMETABLE_HISTORY",
                    "Timetable History",
                    Arrays.asList(
                            new Field("TIMETABLE_ID", "Timetable ID", true, FieldType.NUM), //ref
                            new Field("FLIGHT_STATUS_ID", "Flight Status ID", true, FieldType.NUM), // ref
                            new Field("STATUS_CHANGE_TIME", "Status Change Time", true, FieldType.DATETIME),
                            new Field("STATUS_CHANGE_DESCRIPTION", "Status Change Description", false, FieldType.STRING),
                            new Field("NEW_DEPARTURE", "New Departure", false, FieldType.DATETIME),
                            new Field("NEW_ARRIVAL", "New Arrival", false, FieldType.DATETIME)
                    )
            ));
            put("WORKER_INFO",
                    new Table(
                    "WORKER_INFO",
                    "All Staff",
                    Arrays.asList(
                            new Field("ID", "ID", true, FieldType.AUTOINCREMENT),
                            new Field("FIRST_NAME", "First Name", true, FieldType.STRING),
                            new Field("SECOND_NAME", "Second Name", false, FieldType.STRING),
                            new Field("LAST_NAME", "Last Name", true, FieldType.STRING),
                            new Field("BIRTH_DATE", "Birth Date", true, FieldType.DATETIME),
                            new Field("GENDER", "Gender", true, FieldType.GENDER),
                            new Field("NUMBER_OF_CHILDREN", "Number of Children", true, FieldType.NUM),
                            new Field("START_DATE", "Start Date", true, FieldType.DATETIME),
                            new Field("END_DATE", "End Date", false, FieldType.DATETIME),
                            new Field("SALARY", "Salary", true, FieldType.NUM),
                            new Field("DEPARTMENT_ID", "Department ID", true, FieldType.NUM), // ref
                            new Field("BRIGADE_ID", "Brigade ID", true, FieldType.NUM) // ref
                    )
            ));
    }};

    public final static LinkedHashMap<String, Query> queries = new LinkedHashMap<>()
    {{
            put("List of Workers",
                    new Query("List of Workers",
                            "SELECT FIRST_NAME, SECOND_NAME, LAST_NAME, GENDER, NUMBER_OF_CHILDREN, FLOOR((CURRENT_DATE - START_DATE)/12) AS MONTHS_AT_JOB, SALARY\n" +
                                    "FROM WORKER_INFO\n" +
                                    "WHERE END_DATE IS NULL\n",
                            new LinkedHashMap<String[], QueryFieldType>() {{
                                    put(new String[]{"Sort by", "GENDER", "NUMBER_OF_CHILDREN", "MONTHS_AT_JOB", "SALARY"}, QueryFieldType.ORDER_OPTION);
                            }}
                    ));
            put("List of Managers",
                    new Query("List of Managers",
                            "SELECT FIRST_NAME, SECOND_NAME, LAST_NAME, GENDER, NUMBER_OF_CHILDREN, FLOOR((CURRENT_DATE - START_DATE)/12) AS MONTHS_AT_JOB, SALARY\n" +
                                    "FROM WORKER_INFO\n" +
                                    "JOIN ADMINISTRATOR ON (WORKER_INFO.ID = ADMINISTRATOR.WORKER_ID)\n" +
                                    "WHERE END_DATE IS NULL\n",
                            new LinkedHashMap<String[], QueryFieldType>() {{
                                    put(new String[]{"Sort by", "GENDER", "NUMBER_OF_CHILDREN", "MONTHS_AT_JOB", "SALARY"}, QueryFieldType.ORDER_OPTION);
                            }}
                    ));
            put("Workers per Department",
                    new Query("Workers per Department",
                            "SELECT FIRST_NAME, SECOND_NAME, LAST_NAME, GENDER, NUMBER_OF_CHILDREN, FLOOR((CURRENT_DATE - START_DATE)/12) AS MONTHS_AT_JOB, SALARY, DEPARTMENT_ID\n" +
                                    "FROM WORKER_INFO\n" +
                                    "WHERE END_DATE IS NULL AND (DEPARTMENT_ID = ?)\n",
                            new LinkedHashMap<String[], QueryFieldType>() {{
                                    put(new String[]{"Sort by", "GENDER", "NUMBER_OF_CHILDREN", "MONTHS_AT_JOB", "SALARY"}, QueryFieldType.ORDER_OPTION);
                                    put(new String[]{"Department ID"}, QueryFieldType.NUMBER);
                            }}
                    ));
            put("Brigade on a Plane",
                    new Query("Brigade on a Plane",
                            "SELECT FIRST_NAME, SECOND_NAME, LAST_NAME, FLOOR((CURRENT_DATE - BIRTH_DATE)) AS AGE_IN_DAYS, SALARY\n" +
                                    "FROM WORKER_INFO\n" +
                                    "WHERE END_DATE IS NULL AND BRIGADE_ID = (\n" +
                                    "        SELECT BRIGADE_ID\n" +
                                    "        FROM PLANE\n" +
                                    "        WHERE PLANE_ID = ?\n" +
                                    "    )\n\n",
                            new LinkedHashMap<String[], QueryFieldType>() {{
                                    put(new String[]{"Sort by", "AGE_IN_DAYS", "SALARY"}, QueryFieldType.ORDER_OPTION);
                                    put(new String[]{"Plane ID"}, QueryFieldType.NUMBER);
                            }}
                    ));
            put("Medical Checkups in Select Time Period",
                    new Query("Medical Checkups in Select Time Period",
                            "WITH LATEST_RECORD_FOR_EACH_PILOT AS (\n" +
                                    "    SELECT PILOT_ID, MAX(CHECKUP_DATE) AS LATEST_CHECKUP_DATE\n" +
                                    "    FROM MEDICAL_CHECKUP_HISTORY\n" +
                                    "    WHERE CHECKUP_DATE BETWEEN TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') and TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')\n" +
                                    "    GROUP BY PILOT_ID\n" +
                                    ")\n" +
                                    "SELECT FIRST_NAME, SECOND_NAME, LAST_NAME, GENDER, FLOOR((CURRENT_DATE - BIRTH_DATE)) AS AGE_IN_DAYS, SALARY, LATEST_CHECKUP_DATE\n" +
                                    "FROM LATEST_RECORD_FOR_EACH_PILOT\n" +
                                    "JOIN WORKER_INFO ON (PILOT_ID = WORKER_INFO.ID)\n" +
                                    "JOIN MEDICAL_CHECKUP_HISTORY USING (PILOT_ID)\n" +
                                    "WHERE IS_SUCCESSFUL=? \n",
                            new LinkedHashMap< String[], QueryFieldType>() {{
                                    put(new String[]{"Sort by", "GENDER", "SALARY", "AGE_IN_DAYS"}, QueryFieldType.ORDER_OPTION);
                                    put(new String[]{"Start Date"}, QueryFieldType.DATETIME);
                                    put(new String[]{"End Date"}, QueryFieldType.DATETIME);
                                    put(new String[]{"Successful", "T", "F"}, QueryFieldType.DROPDOWN);
                            }}
                    ));
        put("Planes Bound to the Airport",
                new Query("Planes Bound to the Airport",
                        "SELECT MANUFACTURER, MODEL, PLANE_ID, AIRPORT_NAME\n" +
                                "FROM PLANE_TYPE\n" +
                                "JOIN PLANE ON (PLANE_TYPE.ID = PLANE.PLANE_ID)\n" +
                                "JOIN AIRPORT ON (AIRPORT.ID = PLANE.BASE_AIRPORT_ID)\n" +
                                "WHERE PLANE.BASE_AIRPORT_ID = ? \n" +
                                "ORDER BY PLANE_ID",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Airport ID"}, QueryFieldType.NUMBER);
                        }}
                ));

        put("Planes Currently in the Airport",
                new Query("Planes Currently in the Airport",
                        "WITH LATEST_STATUS_CHANGES AS (\n" +
                                "    SELECT ID, MAX(STATUS_CHANGE_TIME) AS LATEST_CHANGE\n" +
                                "    FROM TIMETABLE\n" +
                                "    JOIN TIMETABLE_HISTORY T on TIMETABLE.ID = T.TIMETABLE_ID\n" +
                                "    GROUP BY ID\n" +
                                "),\n" +
                                "ACTIVE_PLANES AS (\n" +
                                "    SELECT TIMETABLE.ID, PLANE_ID\n" +
                                "    FROM TIMETABLE\n" +
                                "    JOIN TIMETABLE_HISTORY TH on TIMETABLE.ID = TH.TIMETABLE_ID\n" +
                                "    JOIN LATEST_STATUS_CHANGES on TIMETABLE_ID = LATEST_STATUS_CHANGES.ID\n" +
                                "    WHERE FLIGHT_STATUS_ID != 3 AND STATUS_CHANGE_TIME = LATEST_CHANGE AND NEW_DEPARTURE < TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')\n" +
                                "),\n" +
                                "LOCAL_PLANES AS (\n" +
                                "    SELECT PLANE_ID\n" +
                                "    FROM PLANE\n" +
                                "    JOIN PLANE_TYPE PT on PLANE.PLANE_TYPE_ID = PT.ID\n" +
                                "    WHERE PLANE.BASE_AIRPORT_ID = ? AND PLANE_ID NOT IN (SELECT PLANE_ID FROM ACTIVE_PLANES)\n" +
                                ")\n" +
                                "\n" +
                                "SELECT LOCAL_PLANES.PLANE_ID, P.MANUFACTURER, P.MODEL\n" +
                                "FROM LOCAL_PLANES\n" +
                                "JOIN PLANE on LOCAL_PLANES.PLANE_ID = PLANE.PLANE_ID\n" +
                                "JOIN PLANE_TYPE P on PLANE.PLANE_TYPE_ID = P.ID\n" +
                                "\n" +
                                "UNION\n" +
                                "\n" +
                                "SELECT ACTIVE_PLANES.PLANE_ID, P.MANUFACTURER, P.MODEL\n" +
                                "FROM ACTIVE_PLANES\n" +
                                "JOIN TIMETABLE on ACTIVE_PLANES.ID = TIMETABLE.ID\n" +
                                "JOIN TIMETABLE_HISTORY on ACTIVE_PLANES.ID = TIMETABLE_HISTORY.TIMETABLE_ID\n" +
                                "JOIN FLIGHT on TIMETABLE.FLIGHT_ID = FLIGHT.ID\n" +
                                "JOIN ROUTE on FLIGHT.ROUTE_ID = ROUTE.ID\n" +
                                "JOIN PLANE on TIMETABLE.PLANE_ID = PLANE.PLANE_ID\n" +
                                "JOIN PLANE_TYPE P on PLANE.PLANE_TYPE_ID = P.ID\n" +
                                "WHERE ROUTE.ARRIVAL_ID = ? and NEW_ARRIVAL < TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')\n" +
                                "\n" +
                                "ORDER BY PLANE_ID\n",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Time"}, QueryFieldType.DATETIME);
                            put(new String[]{"Airport ID"}, QueryFieldType.NUMBER);
                            put(new String[]{"1"}, QueryFieldType.DUPLICATE);
                            put(new String[]{"0"}, QueryFieldType.DUPLICATE);
                        }}
                ));

        put("Repairs in Select Time Period",
                new Query("Repairs in Select Time Period",
                        "WITH RICH_REPAIR_HISTORY AS (\n" +
                                "    SELECT MANUFACTURER, MODEL, PLANE_ID, COUNT(*) AS AMOUNT_OF_REPAIRS, START_OF_USE_DATE\n" +
                                "    FROM REPAIR_HISTORY\n" +
                                "    JOIN PLANE USING (PLANE_ID)\n" +
                                "    JOIN PLANE_TYPE ON (PLANE.PLANE_TYPE_ID = PLANE_TYPE.ID)\n" +
                                "    WHERE REPAIR_DATE BETWEEN TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') and TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')\n" +
                                "    GROUP BY MANUFACTURER, MODEL, PLANE_ID, START_OF_USE_DATE\n" +
                                "    ORDER BY START_OF_USE_DATE" +
                                ")\n" +
                                "\n" +
                                "SELECT *\n" +
                                "FROM RICH_REPAIR_HISTORY\n" +
                                "WHERE AMOUNT_OF_REPAIRS = ?\n",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Start Date"}, QueryFieldType.DATETIME);
                            put(new String[]{"End Date"}, QueryFieldType.DATETIME);
                            put(new String[]{"Amount of Repairs"}, QueryFieldType.NUMBER);
                        }}
                ));
        put("Flights with Select Route",
                new Query("Flights with Select Route",
                        "WITH LATEST_HISTORY_ENTRY AS (\n" +
                                "    SELECT TIMETABLE_ID, MAX(STATUS_CHANGE_TIME)\n" +
                                "    FROM TIMETABLE_HISTORY\n" +
                                "    GROUP BY TIMETABLE_ID\n" +
                                ")\n" +
                                "SELECT TIMETABLE.ID, (TIMETABLE_HISTORY.NEW_ARRIVAL - TIMETABLE_HISTORY.NEW_DEPARTURE) * 24 * 60 AS FLIGHT_DURATION, COST\n" +
                                "FROM TIMETABLE\n" +
                                "JOIN TIMETABLE_HISTORY ON TIMETABLE.ID = TIMETABLE_HISTORY.TIMETABLE_ID\n" +
                                "JOIN FLIGHT ON TIMETABLE.FLIGHT_ID = FLIGHT.ID\n" +
                                "JOIN ROUTE ON FLIGHT.ROUTE_ID = ROUTE.ID\n" +
                                "WHERE DEPARTURE_ID IN (SELECT ID FROM AIRPORT WHERE CITY = ?) AND\n" +
                                "      ARRIVAL_ID IN (SELECT ID FROM AIRPORT WHERE CITY = ?) AND\n" +
                                "      (TIMETABLE.ID, TIMETABLE_HISTORY.STATUS_CHANGE_TIME) IN ( SELECT * FROM LATEST_HISTORY_ENTRY) AND\n" +
                                "      TIMETABLE_HISTORY.FLIGHT_STATUS_ID != 3\n",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Sort by", "COST", "FLIGHT_DURATION"}, QueryFieldType.ORDER_OPTION);
                            put(new String[]{"Departure City"}, QueryFieldType.TEXT);
                            put(new String[]{"Arrival City"}, QueryFieldType.TEXT);
                        }}
                ));
        put("Canceled Flights",
                new Query("Canceled Flights",
                        "WITH SOLD_SEATS AS (\n" +
                                "    SELECT TIMETABLE_ID, COUNT(TIMETABLE_ID) AS SOLD\n" +
                                "    FROM TICKET\n" +
                                "    WHERE ID NOT IN ( SELECT TICKET_ID FROM TICKET_RETURNS)\n" +
                                "    GROUP BY TIMETABLE_ID\n" +
                                "),\n" +
                                "LATEST_UPDATES AS (\n" +
                                "    SELECT TIMETABLE_ID, MAX(STATUS_CHANGE_TIME) AS LATEST_UPDATE\n" +
                                "    FROM TIMETABLE_HISTORY\n" +
                                "    GROUP BY TIMETABLE_ID\n" +
                                "),\n" +
                                "CANCELED_FLIGHTS AS (\n" +
                                "    SELECT TIMETABLE_ID\n" +
                                "    FROM TIMETABLE_HISTORY\n" +
                                "    WHERE (TIMETABLE_ID, STATUS_CHANGE_TIME) IN ( SELECT * FROM LATEST_UPDATES) AND FLIGHT_STATUS_ID = 3\n" +
                                ")\n" +
                                "\n" +
                                "SELECT TIMETABLE.ID AS TIMETABLE_ID, PLANE_TYPE.SEATS_COUNT - SOLD AS UNSOLD_SEATS, ROUTE.ID AS ROUTE_ID\n" +
                                "FROM TIMETABLE\n" +
                                "JOIN FLIGHT ON TIMETABLE.FLIGHT_ID = FLIGHT.ID\n" +
                                "JOIN ROUTE ON FLIGHT.ROUTE_ID = ROUTE.ID\n" +
                                "JOIN PLANE_TYPE ON FLIGHT.PLANE_TYPE_ID = PLANE_TYPE.ID\n" +
                                "JOIN SOLD_SEATS ON TIMETABLE.ID = SOLD_SEATS.TIMETABLE_ID\n" +
                                "WHERE ARRIVAL_ID IN (SELECT ID FROM AIRPORT WHERE CITY = ?) AND TIMETABLE_ID NOT IN ( SELECT * FROM CANCELED_FLIGHTS)\n",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Sort by", "UNSOLD_SEATS", "ROUTE_ID"}, QueryFieldType.ORDER_OPTION);
                            put(new String[]{"Arrival City"}, QueryFieldType.TEXT);
                        }}
                ));
        put("Flights with Select Plane",
                new Query("Flights with Select Plane",
                        "SELECT FLIGHT.ID, A2.AIRPORT_NAME, A3.AIRPORT_NAME, COST, TIME_OF_DEPARTURE\n" +
                                "FROM FLIGHT\n" +
                                "JOIN ROUTE ON FLIGHT.ROUTE_ID = ROUTE.ID\n" +
                                "JOIN PLANE_TYPE ON FLIGHT.PLANE_TYPE_ID = PLANE_TYPE.ID\n" +
                                "JOIN AIRPORT A2 on A2.ID = ROUTE.ARRIVAL_ID\n" +
                                "JOIN AIRPORT A3 on A3.ID = ROUTE.DEPARTURE_ID\n" +
                                "WHERE PLANE_TYPE_ID = ?\n",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Sort by", "DURATION", "TIME_OF_DEPARTURE", "COST"}, QueryFieldType.ORDER_OPTION);
                            put(new String[]{"Plane Type ID"}, QueryFieldType.NUMBER);
                        }}
                ));
        put("Sales Averages",
                new Query("Sales Averages",
                        "WITH SOLD_SEATS AS (\n" +
                                "    SELECT TIMETABLE_ID, COUNT(TIMETABLE_ID) AS SOLD\n" +
                                "    FROM TICKET\n" +
                                "    WHERE ID NOT IN ( SELECT TICKET_ID FROM TICKET_RETURNS)\n" +
                                "    GROUP BY TIMETABLE_ID\n" +
                                ")\n" +
                                "SELECT ROUTE_ID, COALESCE(AVG(SOLD_SEATS.SOLD), 0) AS SALES_AVERAGE\n" +
                                "FROM TIMETABLE\n" +
                                "JOIN FLIGHT ON TIMETABLE.FLIGHT_ID = FLIGHT.ID\n" +
                                "JOIN ROUTE ON FLIGHT.ROUTE_ID = ROUTE.ID\n" +
                                "LEFT JOIN SOLD_SEATS ON SOLD_SEATS.TIMETABLE_ID = TIMETABLE.ID\n" +
                                "GROUP BY ROUTE_ID\n" +
                                "ORDER BY SALES_AVERAGE",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                        }}
                ));
        put("Flights with Select Parameters",
                new Query("Flights with Select Parameters",
                        "SELECT FLIGHT.ID AS FLIGHT_ID, A1.CITY AS DESTINATION_CITY, A2.CITY AS ARRIVAL_CITY\n" +
                                "FROM FLIGHT\n" +
                                "JOIN ROUTE ON FLIGHT.ROUTE_ID = ROUTE.ID\n" +
                                "JOIN AIRPORT A1 ON ROUTE.DEPARTURE_ID = A1.ID\n" +
                                "JOIN AIRPORT A2 ON ROUTE.ARRIVAL_ID = A2.ID\n" +
                                "WHERE A2.CITY = ? AND FLIGHT.PLANE_TYPE_ID = ? AND FLIGHT.FLIGHT_TYPE_ID = ?\n" +
                                "ORDER BY DURATION\n",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Arrival City"}, QueryFieldType.TEXT);
                            put(new String[]{"Plane Type ID"}, QueryFieldType.NUMBER);
                            put(new String[]{"Flight Type ID"}, QueryFieldType.NUMBER);
                        }}
                ));
        put("Passengers List",
                new Query("Passengers List",
                        "WITH LATEST_UPDATES AS (\n" +
                                "    SELECT TIMETABLE_ID, MAX(STATUS_CHANGE_TIME) AS LATEST_UPDATE\n" +
                                "    FROM TIMETABLE_HISTORY\n" +
                                "    GROUP BY TIMETABLE_ID\n" +
                                "),\n" +
                                "NOT_CANCELLED_FLIGHTS AS (\n" +
                                "    SELECT TIMETABLE_ID, NEW_DEPARTURE\n" +
                                "    FROM TIMETABLE_HISTORY\n" +
                                "    WHERE (TIMETABLE_ID, STATUS_CHANGE_TIME) IN ( SELECT * FROM LATEST_UPDATES) AND FLIGHT_STATUS_ID != 3\n" +
                                ")\n" +
                                "\n" +
                                "SELECT P.FIRST_NAME, P.SECOND_NAME, P.LAST_NAME, P.GENDER,  FLOOR((CURRENT_DATE - P.BIRTH_DATE)) AS AGE_IN_DAYS, AMOUNT_OF_LUGGAGE\n" +
                                "FROM TICKET\n" +
                                "JOIN PASSENGER P on TICKET.PASSENGER_ID = P.ID\n" +
                                "JOIN TIMETABLE T on TICKET.TIMETABLE_ID = T.ID\n" +
                                "JOIN FLIGHT F on T.FLIGHT_ID = F.ID\n" +
                                "JOIN NOT_CANCELLED_FLIGHTS ON (TICKET.TIMETABLE_ID = NOT_CANCELLED_FLIGHTS.TIMETABLE_ID)\n" +
                                "WHERE F.FLIGHT_NUMBER=? AND TICKET.TIMETABLE_ID IN NOT_CANCELLED_FLIGHTS.TIMETABLE_ID AND NEW_DEPARTURE BETWEEN TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') and TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')\n",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Sort by", "AMOUNT_OF_LUGGAGE", "GENDER", "AGE_IN_DAYS"}, QueryFieldType.ORDER_OPTION);
                            put(new String[]{"Flight Number"}, QueryFieldType.TEXT);
                            put(new String[]{"Start Date"}, QueryFieldType.DATETIME);
                            put(new String[]{"End Date"}, QueryFieldType.DATETIME);
                        }}
                ));
        put("Booked Seats",
                new Query("Booked Seats",
                        "SELECT TICKET.TIMETABLE_ID, SEAT, F.COST AS COST, TH.NEW_DEPARTURE AS TIME_OF_DEPARTURE\n" +
                                "FROM TICKET\n" +
                                "JOIN TIMETABLE T on TICKET.TIMETABLE_ID = T.ID\n" +
                                "JOIN TIMETABLE_HISTORY TH on T.ID = TH.TIMETABLE_ID\n" +
                                "JOIN FLIGHT F on T.FLIGHT_ID = F.ID\n" +
                                "WHERE TICKET.ID NOT IN (SELECT TICKET_ID FROM TICKET_RETURNS)\n" +
                                "                AND F.ROUTE_ID = ?\n" +
                                "                AND NEW_DEPARTURE  BETWEEN TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') and TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')\n" +
                                "                AND (TH.TIMETABLE_ID, STATUS_CHANGE_TIME) in (SELECT TIMETABLE_ID, MAX(STATUS_CHANGE_TIME) FROM TIMETABLE_HISTORY GROUP BY TIMETABLE_ID)\n",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Sort by", "COST", "TIME_OF_DEPARTURE"}, QueryFieldType.ORDER_OPTION);
                            put(new String[]{"Route ID"}, QueryFieldType.NUMBER);
                            put(new String[]{"Start Date"}, QueryFieldType.DATETIME);
                            put(new String[]{"End Date"}, QueryFieldType.DATETIME);
                        }}
                ));
        put("Empty Seats",
                new Query("Empty Seats",
                        "SELECT *\n" +
                                "FROM TABLE(EMPTY_SEATS.GET_EMPTY_SEATS(?))\n\n",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Timetable ID"}, QueryFieldType.NUMBER);
                        }}
                ));
        put("Ticket Returns on Select Flight",
                new Query("Ticket Returns on Select Flight",
                        "SELECT TICKET_ID, P.FIRST_NAME, P.SECOND_NAME, P.LAST_NAME, P.BIRTH_DATE, P.GENDER\n" +
                                "FROM TICKET_RETURNS\n" +
                                "JOIN TICKET T on TICKET_RETURNS.TICKET_ID = T.ID\n" +
                                "JOIN PASSENGER P on T.PASSENGER_ID = P.ID\n" +
                                "WHERE TIMETABLE_ID = ?\n",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Sort by", "BIRTH_DATE", "GENDER"}, QueryFieldType.ORDER_OPTION);
                            put(new String[]{"Timetable ID"}, QueryFieldType.NUMBER);
                        }}
                ));
        put("Ticket Returns to Select City",
                new Query("Ticket Returns to Select City",
                        "SELECT TICKET_ID, P.FIRST_NAME, P.SECOND_NAME, P.LAST_NAME, P.BIRTH_DATE, P.GENDER\n" +
                                "FROM TICKET_RETURNS\n" +
                                "JOIN TICKET on TICKET_RETURNS.TICKET_ID = TICKET.ID\n" +
                                "JOIN PASSENGER P on TICKET.PASSENGER_ID = P.ID\n" +
                                "JOIN TIMETABLE on TICKET.TIMETABLE_ID = TIMETABLE.ID\n" +
                                "JOIN FLIGHT on TIMETABLE.FLIGHT_ID = FLIGHT.ID\n" +
                                "JOIN ROUTE on FLIGHT.ROUTE_ID = ROUTE.ID\n" +
                                "JOIN AIRPORT on ROUTE.ARRIVAL_ID = AIRPORT.ID\n" +
                                "WHERE AIRPORT.CITY = ?",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Sort by", "BIRTH_DATE", "GENDER"}, QueryFieldType.ORDER_OPTION);
                            put(new String[]{"City"}, QueryFieldType.TEXT);
                        }}
                ));
        put("Ticket Returns in Time Period",
                new Query("Ticket Returns in Time Period",
                        "SELECT TICKET_ID, P.FIRST_NAME, P.SECOND_NAME, P.LAST_NAME, P.BIRTH_DATE, P.GENDER\n" +
                                "FROM TICKET_RETURNS\n" +
                                "JOIN TICKET on TICKET_RETURNS.TICKET_ID = TICKET.ID\n" +
                                "JOIN PASSENGER P on TICKET.PASSENGER_ID = P.ID\n" +
                                "JOIN TIMETABLE on TICKET.TIMETABLE_ID = TIMETABLE.ID\n" +
                                "JOIN FLIGHT on TIMETABLE.FLIGHT_ID = FLIGHT.ID\n" +
                                "JOIN ROUTE on FLIGHT.ROUTE_ID = ROUTE.ID\n" +
                                "JOIN AIRPORT on ROUTE.ARRIVAL_ID = AIRPORT.ID\n" +
                                "WHERE TIME_OF_RETURN  BETWEEN TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') and TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')",
                        new LinkedHashMap< String[], QueryFieldType>() {{
                            put(new String[]{"Sort by", "BIRTH_DATE", "GENDER"}, QueryFieldType.ORDER_OPTION);
                            put(new String[]{"Start Date"}, QueryFieldType.DATETIME);
                            put(new String[]{"End Date"}, QueryFieldType.DATETIME);
                        }}
                ));
    }};
}
